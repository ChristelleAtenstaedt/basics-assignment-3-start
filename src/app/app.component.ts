import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [
    `
      .white-text {
        color: white;
      }
    `,
  ],
})
export class AppComponent {
  secretPassword = 'Tuna';
  displayDetails = false;
  i = 0;
  val = 0;

  buttonClicks = [];

  toggleDetails() {
    this.displayDetails = !this.displayDetails;
  }

  onClick(val) {
    this.val++;
    this.buttonClicks.push(val);
    console.log(this.val);
  }
  getColor(i) {
    return i >= 4 ? 'blue' : 'transparent';
  }
}
